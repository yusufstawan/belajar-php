<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new Animal("shaun");

echo "Release 0 <br><br>";
echo "Nama Hewan : " . $sheep->name . "<br>";
echo "Jumlah Kaki : " . $sheep->legs . "<br>";
echo "Berdarah Dingin : " . $sheep->cold_blooded . "<br><br>";

echo "Release 1 <br><br>";
$sungokong = new Ape("kera sakti");
echo $sungokong->yell() . "<br>";

$kodok = new Frog("buduk");
echo $kodok->jump() . "<br>";
