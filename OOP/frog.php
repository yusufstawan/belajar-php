<?php

require_once 'animal.php';

class Frog extends Animal
{
  public $name;

  public function __construct($string)
  {
    $this->name = $string;
  }

  public function jump()
  {
    return "hop hop";
  }
}
